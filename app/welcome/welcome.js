'use strict'

angular.module('myApp.welcome',['ngRoute'])

.config(['$routeProvider',function ($routeProvider) {
  $routeProvider.when('/welcome', {
    templateUrl: 'welcome/welcome.html',
    controller: 'WelcomeCtrl'
  });

}])

.controller('WelcomeCtrl',['$scope','CommonProp','$firebase',function ($scope,CommonProp,$firebase) {
  $scope.username = CommonProp.getUser();
    var firebaseObj = new Firebase("https://sweltering-inferno-3905.firebaseio.com/Articles");
    var sync = $firebase(firebaseObj);
    $scope.articles = sync.$asArray();

}]);
