'use strict';

angular.module('myApp.addPost',['ngRoute'])
.config(['$routeProvider',function ($routeProvider) {
  $routeProvider.when('/addPost',{
    templateUrl: 'addPost/addPost.html',
    controller: 'AddPostCtrl'
  });
}])
.controller('AddPostCtrl',['$scope','$firebase','CommonProp',function ($scope,$firebase,CommonProp) {
    var firebaseObj = new Firebase("https://sweltering-inferno-3905.firebaseio.com/Articles");
    var fb = $firebase(firebaseObj);

  $scope.AddPost = function(){
      var title = $scope.article.title;
      var post = $scope.article.post
      //now we have title as post: call Firebase push API to save data to Firebase
      fb.$push({
          title: title,
          post: post,
          emailId: CommonProp.getUser()
        }).then(function(ref){
          console.log("Post added with sucess!");
          $location.path('/welcome');
      }, function(error){
      console.log("Error:", error)});
  }
}]);



//angular.module().config().controller();
