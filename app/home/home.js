'use strict';

angular.module('myApp.home', ['ngRoute','firebase'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home',{
    templateUrl:'home/home.html',
    controller:'HomeCtrl'
  });

}])

//Home controller
.controller('HomeCtrl',['$scope','$location','CommonProp','$firebaseAuth',function($scope,$location,CommonProp,$firebaseAuth){


  var firebaseObj = new Firebase("https://sweltering-inferno-3905.firebaseio.com/");
  var loginObj = $firebaseAuth(firebaseObj);

  $scope.SignIn = function(event){
    event.preventDefault(); //prevent form refresh
    var username = $scope.user.email;
    var password = $scope.user.password;
    //auth logic will be here
    loginObj.$authWithPassword({
      email: username,
      password: password
    }).then(function(user){
      //success callback
      $location.path('/welcome');
      CommonProp.setUser(user.password.email);
      console.log('Autentication sucessful');
    }, function (error) {
      console.log('Autentication failure');
    });
  }


}])

.service('CommonProp',function () {
  var user = '';
  return {
    getUser: function () {
      return user;
    },
    setUser: function (value) {
      user = value;
    }
  };
});
